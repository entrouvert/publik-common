.PHONY: clean name version name fullname

NAME=$(shell basename $(CURDIR))
VERSION=`git describe | sed 's/^debian\///' | sed 's/v//'`
DIST_FILES = Makefile \
			 publik-create-users \
			 publik-create-databases \
			 publik-cluster-link \
			 doc \
			 nginx

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))

clean:
	rm -rf sdist

dist-bzip2: dist
	cd sdist && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 $(NAME)-$(VERSION)

dist: clean
	-mkdir sdist
	rm -rf sdist/$(NAME)-$(VERSION)
	mkdir -p sdist/$(NAME)-$(VERSION)
	for i in $(DIST_FILES); do \
		cp -R "$$i" sdist/$(NAME)-$(VERSION); \
	done

