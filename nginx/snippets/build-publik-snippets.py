#!/usr/bin/env python3
from packaging import version
from jinja2 import Environment, FileSystemLoader, __version__, StrictUndefined

assert version.parse(__version__) < version.parse('2.11')

TEMPLATE = 'publik-application.conf.j2'
MODULES = (
    'authentic2-multitenant',
    'bijoe',
    'chrono',
    'combo',
    'fargo',
    'hobo',
    'passerelle',
    'welco',
)

for module in MODULES:
    loader = FileSystemLoader('.')
    env = Environment(loader=loader, undefined=StrictUndefined)
    template = env.get_template(TEMPLATE)
    with open('publik-%s.conf' % module, 'w') as output:
        output.write(template.render(MODULE=module))
